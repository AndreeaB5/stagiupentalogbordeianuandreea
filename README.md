# README #

 Given a choice of geometric figures, calculate the chosen figure’s area and perimeter. The required lengths will also be input by the user. The user can choose between a circle, a triangle or a rectangle. User input should be taken via console.