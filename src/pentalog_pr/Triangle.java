package pentalog_pr;

public class Triangle {
	
	double a,b,c;
	
	Triangle(double a,double b,double c)
	{
		this.a=a;
		this.b=b;
		this.c=c;
	}

	public double triangle_perimeter(double a,double b,double c)
	{
		double P=a+b+c;
		return P;
	}
	
	
	public double triangle_area(double a,double b,double c)
	{
		double p=(a+b+c)/2;
		double A=Math.sqrt(p*(p-a)*(p-b)*(p-c));
		return A;
	}
}
