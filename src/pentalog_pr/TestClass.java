package pentalog_pr;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
//import java.util.InputMismatchException;
import java.util.Scanner;

public class TestClass {


	private static double read()
	{
	
		//using class java.util.Scanner
		/*try{
		Scanner s=new Scanner(System.in);
		double value=s.nextDouble();
		
		while(value<=0)
		{
			System.out.print("The value introduced is negative or null.Reintroduce the value:");
			value=s.nextDouble();
		}
		
		return value;
		}catch(InputMismatchException e)
		{
			System.out.print("The value introduced is not a number.");
			return -1;
		}*/
		
		//using BufferedReader
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String line = null;
		try {
			line= br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		double val = 0;
		try{		
			val = Double.parseDouble(line);
			while(val<=0)
			{
				System.out.print("The value introduced is negative or null. Type a new  value:");
				try {
					line= br.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
				val = Double.parseDouble(line);
				
			}
			return val;
		}catch(NumberFormatException e){
			System.out.print("The value introduced is not a number.Type a new value:");
			return -1;
		}
	
	
}
	public static void main(String[]args)
	{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner s=new Scanner(System.in);
		int ok=1;
		while(ok==1){
		 System.out.print("Choose between circle ,triangle or rectangle:");
		 String figure_type=null;
		 try 
		 {
			 figure_type = br.readLine();
		 } 
		 catch (IOException e) 
		 {
			e.printStackTrace();
		 }
		 switch(figure_type)
		 {
		 
		 case "circle":
						double radius = 0;
						System.out.print("Radius:");
						radius=read();
						while(radius==-1)
						{
							radius=read();
						}
		 				Circle circle=new Circle(radius);
		 				double P_circle=circle.circle_perimeter(radius);
		 				double A_circle=circle.circle_area(radius);
		 				System.out.println("The perimeter of the circle is:"+P_circle);
		 				System.out.println("The area of the circle is:"+A_circle);
		 				break;
		 				
		 				
		 				
		 case "triangle":double a=0,b=0,c=0;
		 				 boolean valid=false;
		 				 
		 				 while(valid==false)
		 				 {
		 					System.out.print("First length->a:");
			 				a=read();
		 					while(a==-1)
	 						{
		 						a=read();
	 						}
						
		 					System.out.print("Second length->b:");
							b=read();
							while(b==-1)
							{
								b=read();
							}
							
							System.out.print("Third length->c:");
							c=read();
							while(c==-1)
							{
								c=read();
							}
		 					if((a+b)>c&&(a+c)>b&&(b+c)>a)
		 					{
		 						valid=true;
			 					Triangle triangle=new Triangle(a,b,c);
								double P_triangle=triangle.triangle_perimeter(a, b, c);
								double A_triangle=triangle.triangle_area(a, b, c);
								System.out.println("The perimeter of the triangle is:"+P_triangle);
					 			System.out.println("The area of the triangle is:"+A_triangle);
			 				}
		 					else
		 					{
		 						System.out.println(a+","+b+" and " +" can't be the lengths of a triangle.");
		 					}
		 						
		 				 	}
		 				
							break;
								
		 case "rectangle":
							 double length=0;
							 System.out.print("Length:");
							 length=read();
							 while(length==-1)
							 {
								System.out.print("Length:");
								length=read();
							 }
							 double width=0;
							 System.out.print("Width:");
							 width=read();
							 while(width==-1)
							 {
								System.out.print("width:");
								width=read();
							 }
							Rectangle rectangle=new Rectangle(length,width);
							double P_rectangle=rectangle.rectangle_perimeter(length, width);
							double A_rectangle=rectangle.rectangle_area(length, width);
							System.out.println("The perimeter of the rectangle is:"+P_rectangle);
			 				System.out.println("The area of the rectangle is:"+A_rectangle);
			 				break;
			 				
		default:System.out.println("You don't choose any available option.");				
							
		 }	
		 System.out.print("\nEnter 1 if you want to continue or 0 to stop:");
		 ok=s.nextInt();
		 }
		s.close();
	}
}
