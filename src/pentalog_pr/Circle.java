package pentalog_pr;

public class Circle {

	double radius;
	Circle(double radius)
	{
		this.radius=radius;
	}
	
	public double circle_perimeter(double radius)
	{
		double P=2*Math.PI*radius;
		return P;
	}
	
	
	public double circle_area(double radius)
	{
		double A=2*Math.PI*Math.pow(radius, 2);
		return A;
	}
}
