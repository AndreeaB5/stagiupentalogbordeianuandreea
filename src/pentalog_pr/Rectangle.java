package pentalog_pr;

public class Rectangle {

	
	double length,width;
	Rectangle(double length, double width)
	{
		this.length=length;
		this.width=width;
	}
	
	public double rectangle_perimeter(double length, double width)
	{
		double P=2*length+2*width;
		return P;
	}
	
	
	public double rectangle_area(double length, double width)
	{
		double A=length*width;
		return A;
	}
}
